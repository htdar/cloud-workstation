;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (projectile-project-name . "assist")
  (project-bg-colour . "#5a7300")
  (project-fg-colour . "#cfd4dd")
  (project-anon-face . (:foreground "#cfd4dd" :background "#5a7300"))
  (propertized-project-name . (#("assist" 0 6 (face (:foreground "#cfd4dd" :background "#5a7300")))))
  (project-main-org-file . "assist.org")
  (projectile-project-compilation-cmd "docker build -t htdar/assist:1.1 --compress --output type=tar,dest=assist.tar.gz - < Dockerfile")))
