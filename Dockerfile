# use the 'native compilation' version of emacs which has significant performance improvements
# it uses Debian 'buster' for its base image, see: https://akrl.sdf.org/gccemacs.html#org3b574da
FROM andreacorallo/emacs-nativecomp:latest
ARG assist_user=assist
ARG assist_uid=900000000

# install dependencies
RUN apt update && \
    apt install -y --no-install-recommends \
## x11 forwarding for emacs GUI access via Windows
    ssh xauth x11-apps \
    # xorg xauth openbox \
## document conversion & diagram export in emacs
    hugo graphviz plantuml pandoc \
## dependencies for core emacs/terminal functionality
    man-db git curl zsh tmux ripgrep fd-find fzf libvterm-dev && \
    rm -rf /var/lib/apt/lists/*
RUN curl -fsSL https://starship.rs/install.sh | bash -s -- --yes
    # curl -L -O https://github.com/sharkdp/bat/releases/download/v0.15.4/bat_0.15.4_amd64.deb | dpkg --install -

# get latest versions of node & install dependencies for enabling emacs access via browser
RUN curl -sL https://deb.nodesource.com/setup_lts.x | bash - && \
    curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt update && \
    apt install -y --no-install-recommends \
    nodejs yarn \
    make cmake build-essential python3 && \
    rm -rf /var/lib/apt/lists/*
RUN curl -L --output "/usr/local/bin/ttyd" "https://github.com/tsl0922/ttyd/releases/latest/download/ttyd_linux.x86_64" && \
    chmod +x /usr/local/bin/ttyd

# get nerd-font patched version of font
RUN mkdir -p /tmp/fonts && \
    curl -L https://github.com/epk/SF-Mono-Nerd-Font/archive/master.zip -o /tmp/fonts/sf-mono-nerd-fonts.zip && \
    curl -L https://github.com/blaisck/sfwin/archive/master.zip -o /tmp/fonts/sf-fonts.zip && \
    unzip /tmp/fonts/sf-fonts.zip '*.otf' -x 'sfwin-master/SFMono' -d /usr/local/share/fonts && \
    unzip /tmp/fonts/sf-mono-nerd-fonts.zip '*.otf' -d /usr/local/share/fonts && \
    rm -rf /tmp/fonts

# set python3 to the default system python (prevents node cli tool build errors)
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 2 && \
    update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1

# build js cli tools
RUN yarn global add \
    # for generating diagrams using mermaid syntax
    @mermaid-js/mermaid-cli \ 
    --prefix /usr/local

# try and reduce image size
RUN yarn cache clean && \
    rm -rf /usr/share/locale

# create & switch to non-root user for security purposes, create remote mount dir
# use uid 99 by default as this is the only one supported by target instance of k8s
RUN groupadd -g $assist_uid $assist_user && \
    # -l needed to get around large uid bug in Docker https://github.com/moby/moby/issues/5419
    useradd -mlu $assist_uid  -s /bin/zsh -g $assist_uid $assist_user && \
    mkdir -p /local/data && \
    chown $assist_user:$assist_user /local/data
## must use userid rather than username to run or k8s will complain
USER $assist_uid

# set default shell (zsh), env variables and workdir
SHELL [ "zsh", "-c" ]
ENV HOME=/home/$assist_user \
    LANG=en_GB.UTF-8 \
    LC_CTYPE=en_GB.UTF-8 \
    ## enable high colour for terminal emacs, try to get 24-bit colour if possible
    TERM=xterm-256color \
    ### may need to unset COLORTERM if unsupported by terminal (e.g. old versions of PuTTY)
    COLORTERM=truecolor
WORKDIR ${HOME}

# install zsh config framework ('oh-my-zsh') and create base directories
RUN curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh | zsh && \
    echo 'eval "$(starship init zsh)"' >> ~/.zshrc && \
    ## need to create dir
    mkdir -p ${HOME}/.config && \
    mkdir -p /local/data/$assist_user && \
    # make server auth file available to local host
    ln -s ${HOME}/.config/emacs/server /local/data/$assist_user/emacs-server && \
    # symlink org and projects folder to local data
    ln -s ${HOME}/org /local/data/$assist_user/org && \
    ln -s ${HOME}/projects /local/data/$assist_user/projects

# clone repos for emacs config framework ('doom') + personal config
RUN git clone https://gitlab.com/xeijin-dev/doom-config ~/.config/doom && \
    git clone https://github.com/hlissner/doom-emacs ~/.config/emacs

# install emacs packages based on personal config
RUN ${HOME}/.config/emacs/bin/doom -y env && \
    ${HOME}/.config/emacs/bin/doom -y sync

# setup ssh correctly for x11 forwarding graphical apps
RUN printf "ForwardX11 yes\nForwardX11Trusted yes" >> ~/.ssh

# expose volumes
VOLUME /local/data

# emacs daemon port
EXPOSE 8082

# emacs is run in daemon mode to allow opening new editor instances with 'emacsclient'
# this server/client model results in a quicker startup and consumes less resources
# it also enables cross-platform usage in combination with an X server and emacsclient.
# Running the daemon in foreground mode sends any errors to stdout for visibility
CMD [ "emacs", "--fg-daemon" ]
